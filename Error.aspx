﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>后台处理</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        十分抱歉，网站后台出了点问题，<br />
        1。可能是数据库服务器故障；<br />
        2。或者管理员在对服务器进行编辑；<br />
        3。或者您不是通过OA测试服务器登录本系统<br />
        4.....................<br />
        如有需要请联系管理员，节假日不休息 &nbsp; &nbsp; &nbsp;&nbsp; :-)<br />
    
    </div>
    </form>
</body>
</html>
