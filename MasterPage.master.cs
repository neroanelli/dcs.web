﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Label3.Text = Membership.GetNumberOfUsersOnline().ToString();//获取目前在线人数
        if (!IsPostBack)
        {
            //if (Request.UrlReferrer != null)
            //{
            //    //Response.Write("<script>alert('You are from: '+ Request.UrlReferrer.ToString() );</script>" );//"<script>alert('您的权限不能访问趋势，请登录，谢谢！');</script>"
            //    //Response.Write("<script>alert('Request.UrlReferrer.ToString()');</script>");
            //    string Url = Request.UrlReferrer.ToString();
            //    if ((Url != "http://192.168.3.250/Boiler2.aspx") && (Url != "http://192.168.3.250/Boiler1.aspx") && (Url != "http://192.168.3.250/Generator.aspx") && (Url != "http://192.168.3.250/Trubine.aspx"))
            //    {
            //        Response.Write("<script>alert('请先登录OA,通过OA的“工厂监控”链接访问，谢谢合作！');</script>");
            //        Response.Write(Url);
            //        Response.Write("<script   language='javascript'>window.close();</script>");
            //    }
            //    Response.Write(Url + "111111");
            //}
            //else
            //{
            //    Response.Write("URL==NULL");
            //}
            try
            {
                string Url = Request.UrlReferrer.ToString();
                if (Url != null)  //Url不为空
                {
                    //Url不为空 但是是非法盗链
                    //if ((Url != "http://192.168.3.250/Boiler2.aspx") && (Url != "http://192.168.3.250/Boiler1.aspx") && (Url != "http://192.168.3.250/Generator.aspx") && (Url != "http://192.168.3.250/Trubine.aspx") && (Url != "http://192.168.0.55/xscx/scsj.jsp") && (Url != "http://192.168.3.250/Map.aspx") && (Url != "http://192.168.3.250/"))
                    if ((Url.Substring(0, 16) != "http://10.0.2.71") && (Url.Substring(0, 16) != "http://192.168.1"))
                    {
                        Response.Write("<script>alert('谨防盗链，谢谢合作！');</script>");
                        Response.Write(Url);
                        Response.Write("<script language='javascript'>window.close();</script>");
                        Response.Redirect("LoginTest.aspx");
                        Response.Flush();
                        Response.End();
                    }
                }
                else  //Url是空的 ，也就是地址是手工敲进去的
                {
                    Response.Write("<script>alert('请先登录OA,通过OA的“工厂监控”链接访问，谢谢合作！');</script>");
                    Response.Write("<script language='javascript'>window.close();</script>");
                    Response.Redirect("LoginTest.aspx");
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('请先登录OA,通过OA的“工厂监控”链接访问，谢谢合作！');</script>");
                Response.Write("<script language='javascript'>window.close();</script>");
                Response.Redirect("LoginTest.aspx");
                Response.Flush();
                Response.End();
            }
        }
        if (!Data.ReadKey)
        {
            Response.Write("服务器中没有检测到硬件狗，请确认是否正确安装！");
            //Response.Write("<script   language=javascript>alert('服务器中没有检测到硬件狗，请确认是否正确安装！');</script>");   
            Response.Redirect("LoginTest.aspx");
            Response.Flush();
            Response.End();
        }
    }
    protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void ImageButton9_Click(object sender, ImageClickEventArgs e)
    {

    }
}
