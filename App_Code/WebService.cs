﻿using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;


/// <summary>
/// WebService 的摘要说明
/// </summary>
//[WebService(Namespace = "http://192.168.0.22/RDCS/")]
//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WebService : System.Web.Services.WebService {

    public WebService () {

        //如果使用设计的组件，请取消注释以下行 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    /// <summary>
    /// 返回1#窑产量
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string[] GetAlarmData()
    {
        string[] AlarmData = new string[9];
        AlarmData[0] = Data.ytA[30];
        AlarmData[1] = Data.kilnA[2];
        AlarmData[2] = Data.kilnA[35];
        AlarmData[3] = Data.kilnA[1];
        AlarmData[4] = Data.rawA[9];
        AlarmData[5] = Data.kilnA[112];
        AlarmData[6] = "0";
        AlarmData[7] = Data.rawA[72];
        AlarmData[8] = Data.rawA[41];
        return AlarmData;
    }

    [WebMethod]
    public void SetData(string[] OpcData)
    {
        for (int i = 0; i < 22; i++)
        {
            Data.ks[i] = OpcData[i];
        }
        for (int i = 22; i < 124; i++)
        {
            Data.rawA[i-22] = OpcData[i];
        }
        for (int i = 124; i < 188; i++)
        {
            Data.coal[i -124] = OpcData[i];
        }


        for (int i = 188; i < 337; i++)
        {
            Data.kilnB[i - 188] = OpcData[i];
        }

        for (int i = 337; i < 447; i++)
        {
            Data.ytA[i - 337] = OpcData[i];
        }

        for (int i = 447; i < 523; i++)
        {
            Data.snmA[i - 447] = OpcData[i];
        }
        for (int i = 523; i < 598; i++)
        {
            Data.snmB[i - 523] = OpcData[i];
        }
        for (int i = 598; i < 681; i++)
        {
            Data.webrep[i - 598] = OpcData[i];
        }
        for (int i = 681; i < 711; i++)
        {
            Data.webrep2[i - 681] = OpcData[i];
        }


        
        
        
        
        
        //for (int i = 280; i < 371; i++)
        //{
        //    Data.ytA[i - 280] = OpcData[i];
        //}
        //for (int i = 371; i < 382; i++)
        //{
        //    Data.rawB[i - 371] = OpcData[i];
        //}
        //for (int i = 382; i < 404; i++)
        //{
        //    Data.rawB[i - 336] = OpcData[i];
        //}
        //Data.kilnB[43] = OpcData[404];
        //for (int i = 405; i < 423; i++)
        //{
        //    Data.coal[i - 405] = OpcData[i];
        //}
        //for (int i = 423; i < 442; i++)
        //{
        //    Data.coal[i - 392] = OpcData[i];
        //}
        //Data.coal[66] = OpcData[442];
        //for (int i = 0; i < 25; i++)
        //{
        //    Data.ks[i] = OpcData[i];
        //}
        //for (int i = 25; i <143; i++)
        //{
        //    Data.raw[i-25] = OpcData[i];
        //}
        //for (int i = 143; i < 287; i++)
        //{
        //    Data.kiln[i-143] = OpcData[i];
        //}
        //for (int i = 287; i < 378; i++)
        //{
        //    Data.yt[i-287] = OpcData[i];
        //}
        //for (int i = 378; i < 504; i++)
        //{
        //    Data.raw2[i-378] = OpcData[i];
        //}
        //for (int i = 504; i < 630; i++)
        //{
        //    Data.kiln2[i -504] = OpcData[i];
        //}
        //for (int i = 630; i < 728; i++)
        //{
        //    Data.yt2[i - 630] = OpcData[i];
        //}
        //for (int i = 728; i < 873; i++)
        //{
        //    Data.coal[i - 728] = OpcData[i];
        //}
 
    }
}

