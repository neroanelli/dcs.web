﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Odbc;
using System.Data.Sql;

/// <summary>
/// IFixData 的摘要说明
/// </summary>
public class IFixData
{
    OdbcConnection RealConnection;
    //OdbcConnection HisConnection;
    public IFixData(string ConnectionString)
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
        string RealConnectionString = ConnectionString;
        RealConnection = new OdbcConnection(RealConnectionString);
        RealConnection.Open();
        
    }

        
        public DataSet GetRealData()
        {
            //string RealConnectionSting = "DSN=FIX Dynamics Real Time Data";//实时数据库连接字符串
            ////string HisConnectionSting = "DSN=FIX Dynamics Historical Data";//历史数据库连接字符串
            //OdbcConnection RealConnection;//实时数据库连接
            ////OdbcConnection HisConnection;//历史数据库连接
            //DataSet errds = new DataSet();

            try
            {
                //RealConnection = new OdbcConnection(RealConnectionSting);
                //RealConnection.Open();

                OdbcDataAdapter adapter = new OdbcDataAdapter("select A_CV from FIX", RealConnection);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "FIX");
                adapter.Dispose();
                return ds;
                
               // RealConnection..Close();
            }
            catch (Exception ex)
            {
                return new DataSet();
                

            }

        }
        

	}

    
