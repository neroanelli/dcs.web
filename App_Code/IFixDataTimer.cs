﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Timers;
using System.Data.Odbc;
using System.Data.Sql;

/// <summary>
/// IFixDataTimer 的摘要说明
/// </summary>
public  class IFixDataTimer
{
    static OdbcConnection RealConnection;
    //OdbcConnection HisConnection;
    public static string[] data=new string[100];

    //public static void main(string[] args)
    //{
    //    //IFixDataTimer IDT;
    //    //IDT=new IFixDataTimer("DSN=FIX Dynamics Real Time Data");
        

    //    OdbcDataAdapter adapter = new OdbcDataAdapter("select * from FIX", RealConnection);
    //    DataSet ds = new DataSet();
    //    adapter.Fill(ds, "FIX");
    //    adapter.Dispose();
    //    for (int i = 0; i < 100; i++)
    //    {
    //        Console.WriteLine("" + ds.Tables[0].Rows[i]["A_CV"].ToString() + "!");
    //    }

    //}
    public static void main(string[] args) 
    {
        string RealConnectionString = "DSN=FIX Dynamics Real Time Data";
        RealConnection = new OdbcConnection(RealConnectionString);
        RealConnection.Open();

        System.Timers.Timer t = new System.Timers.Timer();//实例化Timer类，设置间隔时间为10000毫秒； 
        t.Elapsed += new ElapsedEventHandler(GetRealDataTimer);//到达时间的时候执行事件；
        t.Interval = 800;
        t.AutoReset = true;//设置是执行一次（false）还是一直执行(true)； 
        t.Enabled = true;//是否执行System.Timers.Timer.Elapsed事件；
        t.Start();
        GC.KeepAlive(t);
    }
   
    public  IFixDataTimer(string ConnectionString)
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
        string RealConnectionString = ConnectionString;
        RealConnection = new OdbcConnection(RealConnectionString);
        RealConnection.Open();

        System.Timers.Timer t = new System.Timers.Timer();//实例化Timer类，设置间隔时间为10000毫秒； 
        t.Elapsed += new ElapsedEventHandler(GetRealDataTimer);//到达时间的时候执行事件；
        t.Interval = 800;
        t.AutoReset = true;//设置是执行一次（false）还是一直执行(true)； 
        t.Enabled = true;//是否执行System.Timers.Timer.Elapsed事件；
        t.Start();
        GC.KeepAlive(t);

        //public void theout(object source, System.Timers.ElapsedEventArgs e) 
        //{ 
        //MessageBox.Show("OK!"); 
        //}
        
    }

    private static void GetRealDataTimer(object source,ElapsedEventArgs e)
    {
        try
        {
            OdbcDataAdapter adapter = new OdbcDataAdapter("select A_CV from FIX", RealConnection);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "FIX");
            
            for (int i = 0; i < 100; i++)
            {
                data[i] = ds.Tables[0].Rows[i]["A_CV"].ToString();
            }
            adapter.Dispose();
            // RealConnection..Close();
            
        }
        catch (Exception ex)
        {
            for (int i = 0; i < 100; i++)
            {
                data[i] = "???";
            }
            
        }
    }

    public string[] GetRealDataTimer()
    {
        try
        {
            OdbcDataAdapter adapter = new OdbcDataAdapter("select * from FIX", RealConnection);
            DataSet ds = new DataSet();
            adapter.Fill(ds, "FIX");

            for (int i = 0; i < 100; i++)
            {
                data[i] = ds.Tables[0].Rows[i]["A_CV"].ToString();
            }
            adapter.Dispose();
            // RealConnection..Close();
            return data;
        }
        catch (Exception ex)
        {
            for (int i = 0; i < 100; i++)
            {
                data[i] = "???";
            }
            return data;
        }
    }

        
        public DataSet GetRealData()
        {
            //string RealConnectionSting = "DSN=FIX Dynamics Real Time Data";//实时数据库连接字符串
            ////string HisConnectionSting = "DSN=FIX Dynamics Historical Data";//历史数据库连接字符串
            //OdbcConnection RealConnection;//实时数据库连接
            ////OdbcConnection HisConnection;//历史数据库连接
            //DataSet errds = new DataSet();

            try
            {
                //RealConnection = new OdbcConnection(RealConnectionSting);
                //RealConnection.Open();
               
                OdbcDataAdapter adapter = new OdbcDataAdapter("select * from FIX",RealConnection);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "FIX");
                adapter.Dispose();
                return ds;
                
               // RealConnection..Close();
            }
            catch (Exception ex)
            {
                return new DataSet();
                

            }

        }
        
	public IFixDataTimer()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
	}
   
}

//System.Timers.Timer t = new System.Timers.Timer(10000);//实例化Timer类，设置间隔时间为10000毫秒； 
//t.Elapsed = new System.Timers.ElapsedEventHandler(theout);//到达时间的时候执行事件； 
//t.AutoReset = true;//设置是执行一次（false）还是一直执行(true)； 
//t.Enabled = true;//是否执行System.Timers.Timer.Elapsed事件； public void theout(object source, System.Timers.ElapsedEventArgs e) 
//{ 
//MessageBox.Show("OK!"); 
//}
