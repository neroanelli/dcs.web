﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace com.conch.Dataonline
{
    /// <summary>
    /// CSVReader 的摘要说明
    /// 用于读取CSV文件
    /// </summary>
    public class CSVReader
    {

        private string Path;
        private DataSet Ds;

        public DataSet ds
        {
            get { return Ds; }
        }
        
        public CSVReader(string path)
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
            this.Path = path;
            Ds = new DataSet();
            read();
        }

        private void read()
        {
            int intColCount = 0;
            DataTable mydt = new DataTable("myTableName");
            Ds.Tables.Add(mydt);
            DataColumn mydc;
            DataRow mydr;
            mydt.Columns.Add(new DataColumn("Reasona", typeof(string)));
            mydt.Columns.Add(new DataColumn("Reasonb", typeof(string)));
            mydt.Columns.Add(new DataColumn("Reasonc", typeof(string)));

            string strpath = this.Path;
            string strline;
            string[] aryline;

            System.IO.StreamReader mysr = new System.IO.StreamReader(strpath, System.Text.Encoding.Default);

            while ((strline = mysr.ReadLine()) != null)
            {
                aryline = strline.Split(new char[] { ','});

                intColCount = aryline.Length;
                //for (int i = 0; i < aryline.Length; i++)
                //{
                //    //mydc = new DataColumn(aryline[i]);
                //    //mydt.Columns.Add(mydc);
                //    mydr = mydt.NewRow();
                //}

                mydr = mydt.NewRow();
                for (int i = 0; i < intColCount; i++)
                {
                    mydr[i] = aryline[i];
                }
                mydt.Rows.Add(mydr);
                //Ds.Tables.Add(mydt);
            }
            mysr.Close();
        }
    }
}
