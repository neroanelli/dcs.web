﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// LoginData 的摘要说明
/// </summary>
public class LoginData
{
    public string Login_UserID;//登录人员的ID，由登录界面或者输入连接获得
    public string Login_Code;
    public ArrayList Login_Region = new ArrayList();//该ID权限所属，Group，Zone，Company，根据UserID从数据查询得出
    public ArrayList Attribute = new ArrayList();//该ID权限所属具体属性，All,AH,HN.....,AH_WH,AH_BMS,HN_SM,HN_SF等


    public bool CheckByUserID(string UserID,string UserCode)//成功 返回true
    {
        bool dml;
        Login_UserID = UserID;
        Login_Code = UserCode;
        dml = DML();
        return  dml;

    }

    private bool DML()
    {
        string test;
        bool dml=false;
        SqlParameter myparm = new SqlParameter();
        myparm.ParameterName = "@inUserID";
        myparm.SqlDbType = SqlDbType.Char;
        myparm.Size = 30; ;
        myparm.Value = Login_UserID;

        string sqlexec ="FindUserCode";
        SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, sqlexec, myparm);
        while (mydr.Read())
        {
            test = mydr["code"].ToString().Trim();
            if (Login_Code.Equals(mydr["code"].ToString().Trim()))
            {
                dml = true;
            }
            //else
            //{
            //    dml = false;
            //}
            //Login_Region.Add(mydr["Region"].ToString());
            //Attribute.Add(mydr["Attribute"].ToString());
        }
        mydr.Close();
        return dml;
    }
	public LoginData()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
	}
}
