﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Odbc;
using System.Data.Sql;


/// <summary>
/// Data 的摘要说明
/// </summary>
/// string myConnStr = "DSN=football";//建立系统DSN
   //OdbcConnection myConnection = new OdbcConnection(myConnStr);
   //myConnection.Open();
   //OdbcDataAdapter adapter =new OdbcDataAdapter("select * from Admin",myConnection);//创建适配器
   //DataSet ds =new DataSet();//创建数据集
   //adapter.Fill(ds,"Admin");//填充
   //DataGrid1.DataSource=ds;//显示在网格
   //DataGrid1.DataBind();

public  class Data
{
    public static bool ReadKey=false;
    public static int OnlineNO=0;//初始为0，记录在线人数
    //public static  bool Con=true;//初始为true  可以访问历史数据库；false则对历史数据库加锁
    public static string[] Boiler1=new string[67];//1线PH+AQC锅炉系统67个点 模拟量+开关量
    public static string[] Boiler2=new string[68];//2线PH+AQC锅炉系统68个点 模拟量+开关量
    public static string[] Trubine=new string[92];//汽轮机系统92个点 模拟量+开关量 76+16
    public static string[] DEH=new string[23];//发电机系统23个点 模拟量20+开关量3
    public static string[] Test1=new string[200];//用于测试系统压力1000-228=772
    public static string[] Test2 = new string[200];//用于测试系统压力1000-228=772 772-200=572
    public static string[] Test3 = new string[200];//用于测试系统压力1000-228=772 772-200=572 572-200=372
    public static string[] Test4 = new string[172];
    public static string[] WebSData=new string[250];//WebSerivce填写数据
    public static string[] coal = new string[64];//0-200 怀宁煤磨67个点
    public static string[] coal_tooltips = new string[64];//怀宁煤磨67个点标签
    public static string[] coal_Numics = new string[64];//怀宁煤磨67个点标签
    public static string[] kiln = new string[144];//201-400

    public static string[] kilnA = new string[122];//201-400  清新烧成A线144个点
    public static string[] kilnA_tooltips = new string[122];
    public static string[] kilnA_Numics = new string[122];

    public static string[] kilnB = new string[149];//201-400  清新烧成B线144个点
    public static string[] kilnB_tooltips = new string[149];
    public static string[] kilnB_Numics = new string[149];

    public static string[] kiln2 = new string[120];//201-400
    public static string[] ks = new string[22];//401-600 怀宁矿山23个点
    public static string[] ks_tooltips = new string[22];
    public static string[] ks_Numics = new string[22];
    public static string[] raw = new string[118];//601-800

    public static string[] rawA = new string[102];//601-800 清新原料A线118个点
    public static string[] rawA_tooltips = new string[102];
    public static string[] rawA_Numics = new string[102];

    public static string[] rawB = new string[101];//601-800 清新原料B线118个点
    public static string[] rawB_tooltips = new string[101];
    public static string[] rawB_Numics = new string[101];

    public static string[] raw2 = new string[126];//601-800
    public static string[] snm = new string[245];//801-1000
    public static string[] yt = new string[91];

    public static string[] ytA = new string[110]; //清新窑头A线83个点
    public static string[] ytA_tooltips = new string[110];
    public static string[] ytA_Numics = new string[110];

    public static string[] ytB = new string[83]; //清新窑头B线83个点
    public static string[] ytB_tooltips = new string[83];
    public static string[] ytB_Numics = new string[83];

    public static string[] snmA = new string[76]; //清新窑头B线83个点
    public static string[] snmA_tooltips = new string[76];
    public static string[] snmA_Numics = new string[76];

    public static string[] snmB = new string[75]; //清新窑头B线83个点
    public static string[] snmB_tooltips = new string[75];
    public static string[] snmB_Numics = new string[75];

    public static string[] yt2 = new string[98];
    public static string[] webrep = new string[83];//主机设备信号A线。
    public static string[] webrep2 = new string[30];//主机设备信号B线。

   


    public Data()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
    }

#region  从WebService读取实时数据
    public static void GetRealDataFromWebService()
    {
        for (int i = 0; i < 56; i++) //将前56个点放入Boiler1数组中 模拟量
        {
            Boiler1[i] = WebSData[i];
        }
        for (int i = 209; i < 220; i++) //从第210项开始的11个开关量送入Boiler1数组
        {
            Boiler1[i - 153] = WebSData[i];
        }

        for (int j = 56; j < 113; j++) //将57-113的57个点放入Boiler2数组中 模拟量
        {
            Boiler2[j - 56] = WebSData[j];
        }
        for (int j = 220; j < 231; j++) //从第220项开始的11个开关量送入Boiler2数组
        {
            Boiler2[j - 163] = WebSData[j];
        }

        for (int k = 113; k < 189; k++) //将114-189的76个点放入Trubine数组中 模拟量
        {
            Trubine[k - 113] = WebSData[k];
        }


        for (int m = 189; m < 209; m++) //将190-109的20个点放入DEH数组中 模拟量
        {
            DEH[m - 189] = WebSData[m];
        }

        for (int k = 231; k < 247; k++) //从第232-247项的16个开关量送入Trubine数组
        {
            Trubine[k - 155] = WebSData[k];
        }

        for (int H = 247; H < 250; H++) //从第248-250项的3个开关量送入DEH数组
        {
            DEH[H - 227] = WebSData[H];
        }
    }
#endregion

    #region  实时数据库的数据集
    /// <summary>
    /// 实时数据库的数据集
    /// </summary>
    /// <returns></returns>
    public static void GetRealData() 
    {
        //string RealConnectionSting = "DSN=FIX Dynamics Real Time Data";//实时数据库连接字符串
        string RealConnectionSting = ConfigurationSettings.AppSettings["RealDataConnectionString"];//从web.config中读出数据库连接字符串
        //string HisConnectionSting = "DSN=FIX Dynamics Historical Data";//历史数据库连接字符串
        OdbcConnection RealConnection;//实时数据库连接
        //OdbcConnection HisConnection;//历史数据库连接
        RealConnection = new OdbcConnection(RealConnectionSting);
        
        OdbcDataAdapter adapter = new OdbcDataAdapter("select A_CV from FIX", RealConnection);
        DataSet ds = new DataSet();
        RealConnection.Open();// 11.19日修改
        adapter.Fill(ds, "FIX");
        RealConnection.Close();// 11.19日修改
        adapter.Dispose();
        for (int i = 0; i < 56; i++) //将前56个点放入Boiler1数组中 模拟量
        {
            Boiler1[i] = ds.Tables[0].Rows[i]["A_CV"].ToString();
        }
        for (int i = 209; i < 220;i++ ) //从第210项开始的11个开关量送入Boiler1数组
        {
            Boiler1[i-153] = ds.Tables[0].Rows[i]["A_CV"].ToString();
        }

        for (int j = 56; j < 113; j++) //将57-113的57个点放入Boiler2数组中 模拟量
        {
            Boiler2[j-56] = ds.Tables[0].Rows[j]["A_CV"].ToString();
        }
        for (int j = 220; j < 231; j++) //从第220项开始的11个开关量送入Boiler2数组
        {
            Boiler2[j - 163] = ds.Tables[0].Rows[j]["A_CV"].ToString();
        }

        for (int k = 113; k < 189; k++) //将114-189的76个点放入Trubine数组中 模拟量
        {
            Trubine[k - 113] = ds.Tables[0].Rows[k]["A_CV"].ToString();
        }
        

        for (int m = 189; m < 209; m++) //将190-109的20个点放入DEH数组中 模拟量
        {
            DEH[m - 189] = ds.Tables[0].Rows[m]["A_CV"].ToString();
        }

        for (int k = 231; k < 247; k++) //从第232-247项的16个开关量送入Trubine数组
        {
            Trubine[k - 155] = ds.Tables[0].Rows[k]["A_CV"].ToString();
        }

        for (int H = 247; H < 250; H++) //从第248-250项的3个开关量送入DEH数组
        {
            DEH[H - 227] = ds.Tables[0].Rows[H]["A_CV"].ToString();
        }
        //for (int j = 62; j < 125; j++) //将63-125的点放入Boiler2数组中 模拟量+开关量 63个
        //{ 
        //    Boiler2[j - 62] = ds.Tables[0].Rows[j]["A_CV"].ToString();
        //}
        //for (int k = 126; k < 199; k++) // 将前126-199的点放入Trubine数组中 模拟量+开关量 74个
        //{
        //    Trubine[k - 126] = ds.Tables[0].Rows[k]["A_CV"].ToString();
        //}
        //for (int m = 200; m < 228; m++)//将前200-228的点放入DEH数组中 模拟量+开关量 29个
        //{
        //    DEH[m - 200] = ds.Tables[0].Rows[m]["A_CV"].ToString();
        //}
        //return ds;
        ds.Dispose();
    }
    #endregion
    public static DataSet GetRealData1()
    {
        //string RealConnectionSting = "DSN=FIX Dynamics Real Time Data";//实时数据库连接字符串
        string RealConnectionSting = ConfigurationSettings.AppSettings["RealDataConnectionString"];//从web.config中读出数据库连接字符串
        //string HisConnectionSting = "DSN=FIX Dynamics Historical Data";//历史数据库连接字符串
        OdbcConnection RealConnection;//实时数据库连接
        //OdbcConnection HisConnection;//历史数据库连接
        RealConnection = new OdbcConnection(RealConnectionSting);

        OdbcDataAdapter adapter = new OdbcDataAdapter("select A_CV from FIX", RealConnection);
        DataSet ds = new DataSet();
        RealConnection.Open();// 11.19日修改
        adapter.Fill(ds, "FIX");
        RealConnection.Close();// 11.19日修改
        adapter.Dispose();
        //for (int i = 0; i < 62; i++) //将前62个点放入Boiler1数组中 模拟量+开关量
        //{
        //    Boiler1[i] = ds.Tables[0].Rows[i]["A_CV"].ToString();
        //}
        //for (int j = 62; j < 125; j++) //将63-125的点放入Boiler2数组中 模拟量+开关量 63个
        //{ 
        //    Boiler2[j - 62] = ds.Tables[0].Rows[j]["A_CV"].ToString();
        //}
        //for (int k = 126; k < 199; k++) // 将前126-199的点放入Trubine数组中 模拟量+开关量 74个
        //{
        //    Trubine[k - 126] = ds.Tables[0].Rows[k]["A_CV"].ToString();
        //}
        //for (int m = 200; m < 228; m++)//将前200-228的点放入DEH数组中 模拟量+开关量 29个
        //{
        //    DEH[m - 200] = ds.Tables[0].Rows[m]["A_CV"].ToString();
        //}
        return ds;
        //ds.Dispose();
    }

#region 将结果集放置到static 数组中
    private void InputArray(DataSet ds)
    { 
          for(int i=0;i<62;i++) //将前62个点放入Boiler1数组中 模拟量+开关量
          {
              Boiler1[i] = ds.Tables[0].Rows[i]["A_CV"].ToString();
          }
          for(int j=62;j<125;j++) //将63-125的点放入Boiler2数组中 模拟量+开关量 63个
          {
              Boiler2[j - 62] = ds.Tables[0].Rows[j]["A_CV"].ToString();
          }
          for (int k = 126; k < 199;k++ ) // 将前126-199的点放入Trubine数组中 模拟量+开关量 74个
          {
              Trubine[k - 126] = ds.Tables[0].Rows[k]["A_CV"].ToString();
          }
          for (int m = 200; m < 228;m++ )//将前200-228的点放入DEH数组中 模拟量+开关量 29个
          {
              DEH[m - 200] = ds.Tables[0].Rows[m]["A_CV"].ToString(); 
          }
    }
#endregion

    #region 返回历史数据集
    /// <summary>
    /// 返回历史数据集
    /// </summary>
    /// <returns></returns>
    public static DataSet GetHisData(string Tag_Name,string StartTime,string EndTime,string Interval)//StartTime:"2007-09-06 00:00:00" Interval:"00:00:05" 
    {
        //if (Con)//初始为true  可以访问历史数据库
        //{
        //    Con = false;//对别的访问数据库的事件加锁
            //string HisConnectionString = "DSN=FIX Dynamics Historical Data";//历史数据库连接字符串
            string HisConnectionString = ConfigurationSettings.AppSettings["HisDataConnectionString"];//从web.config中读出数据库连接字符串
            OdbcConnection HisConnection;//历史数据库连接
            string QueryString;
            string TN; string ST; string ET; string Inter;
            TN = Tag_Name; ST = StartTime; ET = EndTime; Inter = Interval;
            if (ST == "")
            {
                ST = "2008-02-25 09:50:00";
            }
            if (ET == "")
            {
                ET = "2008-02-25 10:00:00";
            }
            if (Inter == "")
            {
                Inter = "00:00:10";
            }
            if (TN == "")
            {
                //QueryString = "select TAG,VALUE,DATETIME,INTERVAL from FIX where INTERVAL='" + Inter + "'AND (DATETIME>={ts'" + ST + "'} AND DATETIME<={ts'" + ET + "'})";
                QueryString = "select VALUE,DATETIMEL from FIX where TAG='RAC4211/PV1'AND INTERVAL='" + Inter + "'AND (DATETIME>={ts'" + ST + "'} AND DATETIME<={ts'" + ET + "'})";

            }
            else
            {
                //QueryString = "select TAG,VALUE,DATETIME,INTERVAL from FIX where TAG='" + TN + "'AND INTERVAL='" + Inter + "'AND (DATETIME>={ts'" + ST + "'} AND DATETIME<={ts'" + ET + "'})";
                QueryString = "select VALUE,DATETIME from FIX where TAG='" + TN + "'AND INTERVAL='" + Inter + "'AND (DATETIME>={ts'" + ST + "'} AND DATETIME<={ts'" + ET + "'})";
            }
            DataSet ds = new DataSet();
            try
            {
                HisConnection = new OdbcConnection(HisConnectionString);
                HisConnection.Open();
                OdbcDataAdapter adapter = new OdbcDataAdapter(QueryString, HisConnection);
                adapter.Fill(ds, "FIX");
                adapter.Dispose();
                HisConnection.Close();
                return ds;
            }
            catch (Exception ec)
            {
                return ds;
            }
        //    finally
        //    {
        //        Con = true;//释放对历史数据库的锁
        //    }
        //}
        //else   //若历史数据库访问权限加锁 则返回空
        //{
        //    DataSet ds = new DataSet();
        //    return ds;
        //}
        
    }
        #endregion

    #region  根据IFIX数据库格式返回时间查询字符串  格式 YYYY-MM-DD HH:MM:SS
    /// <summary>
    /// 根据IFIX数据库格式返回时间查询字符串  格式 YYYY-MM-DD HH-MM-SS
    /// </summary>
    /// <returns></returns>
    public static string GetTime(string year,string month,string day,string hour,string minute,string second)
    {
        string Year; string Month; string Day; string Hour; string Minute; string Second;
        Year = year; Month = month; Day = day; Hour = hour; Minute = minute; Second = second;
        return Year + "-" + Month + "-" + Day + " " + Hour + ":" + Minute + ":" + Second;
    }
    #endregion

#region  根据开始结束时间计算出700个点的Interval时间
    /// <summary>
    /// 返回string型查询时间间隔
    /// </summary>
    /// <returns></returns>
    public static string GetInterval(string startMonth, string startDay, string startHour, string startMinute, string startSecond, string endMonth, string endDay, string endHour, string endMinute, string endSecond)
    {
        int startM = Convert.ToInt32(startMonth);
        int startD = Convert.ToInt32(startDay);
        int startH = Convert.ToInt32(startHour);
        int startMin = Convert.ToInt32(startMinute);
        int startS = Convert.ToInt32(startSecond);
        int endM = Convert.ToInt32(endMonth);
        int endD = Convert.ToInt32(endDay);
        int endH = Convert.ToInt32(endHour);
        int endMin = Convert.ToInt32(endMinute);
        int endS = Convert.ToInt32(endSecond);
        //(month * 2592000) + (day * 86400) + (hour * 3600) + (minute * 60);
        int Interv;
        Interv = ((endM - startM) * 2592000 + (endD - startD) * 86400 + (endH - startH) * 3600 + (endMin - startMin) * 60 + (endS - startS)) / 700 + 1;
        if (Interv < 60)
        {
            string inte;
            if (Interv < 10)
            {
                inte = "0" + Convert.ToString(Interv);
            }
            else
            {
                inte = Convert.ToString(Interv);
            }
            return "00:00:" + inte;
        }
        else
        {
            if (Interv >= 60 && Interv < 3600)
            {
                int Minute = Interv / 60;
                int Second = Interv - (Minute * 60);
                string min, sec;
                if (Minute < 10)
                {
                    min = "0" + Convert.ToString(Minute);
                }
                else
                {
                    min = Convert.ToString(Minute);
                }

                if (Second < 10)
                {
                    sec = "0" + Convert.ToString(Second);
                }
                else 
                {
                    sec = Convert.ToString(Second);
                }
                  

                return "00:" + min + ":" + sec;
            }
            else
            {
                if (Interv >= 3600)
                {
                    int Hour = Interv / 3600;
                    int Minute = (Interv - (Hour * 3600)) / 60;
                    int Second = Interv - (Hour * 3600) - (Minute * 60);
                    string hou, min, sec;

                    if(Hour<10)
                    {
                        hou = "0" + Convert.ToString(Hour); 
                    }
                    else
                    {
                        hou = Convert.ToString(Hour);
                    }

                    if (Minute < 10)
                    {
                        min = "0" + Convert.ToString(Minute);
                    }
                    else
                    {
                        min = Convert.ToString(Minute);
                    }

                    if (Second < 10)
                    {
                        sec = "0" + Convert.ToString(Second);
                    }
                    else
                    {
                        sec = Convert.ToString(Second);
                    }

                    return hou + ":" + min + ":" + sec;
                }
                else
                {
                    return "00:30:00";
                }
            }
        }
    }

#endregion

}
