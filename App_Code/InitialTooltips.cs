﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using com.conch.Dataonline;

/// <summary>
/// InitialTooltips 的摘要说明
/// </summary>
namespace com.conch.Init
{
    public class InitialTooltips
    {
        public InitialTooltips()
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
        }

        /// <summary>
        /// 初始化每个每页标签的tooltips，从c盘根目录的read.csv文件读取
        /// </summary>
        public static void SetTooltips()
        {
            DataSet dds = new DataSet();
            CSVReader csvr = new CSVReader("C://read_HYS.csv");
            dds = csvr.ds;

            //dds.Tables["myTableName"].Rows[0][2]="dd";

            //for (int i = 0; i < 23; i++)
            //{
            //    Data.ks[i] = dds.Tables["myTableName"].Rows[i][2].ToString();
            //    if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
            //    {
            //        Data.ks_tooltips[i] = "null";
            //    }
            //    else
            //    {
            //        Data.ks_tooltips[i] = dds.Tables["myTableName"].Rows[i][1].ToString();
            //    }
            //}
            for (int i = 0; i < 22; i++)
            {
                Data.ks_Numics[i] = dds.Tables["myTableName"].Rows[i][2].ToString();
                Data.ks[i] = i.ToString();
                if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                {
                    Data.ks_tooltips[i ] = "null";
                }
                else
                {
                    Data.ks_tooltips[i] = dds.Tables["myTableName"].Rows[i][1].ToString();
                }
            }
            for (int i = 22; i < 124; i++)
            {
                Data.rawA_Numics[i - 22] = dds.Tables["myTableName"].Rows[i][2].ToString();
                Data.rawA[i - 22] = i.ToString();
                if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                {
                    Data.rawA_tooltips[i - 22] = "null";
                }
                else
                {
                    Data.rawA_tooltips[i - 22] = dds.Tables["myTableName"].Rows[i][1].ToString();
                }
            }
            for (int i = 124; i < 188; i++)
            {
                Data.coal_Numics[i - 124] = dds.Tables["myTableName"].Rows[i][2].ToString();
                Data.coal[i - 124] = i.ToString();
                if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                {
                    Data.coal_tooltips[i - 124] = "null";
                }
                else
                {
                    Data.coal_tooltips[i - 124] = dds.Tables["myTableName"].Rows[i][1].ToString();
                }
            }

            for (int i = 188; i < 337; i++)
            {
                Data.kilnB_Numics[i-188] = dds.Tables["myTableName"].Rows[i][2].ToString();
                Data.kilnB[i - 188] = i.ToString();
                if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                {
                    Data.kilnB_tooltips[i-188] = "null";
                }
                else
                {
                    Data.kilnB_tooltips[i-188] = dds.Tables["myTableName"].Rows[i][1].ToString();
                }
            }
            for (int i = 337; i < 447; i++)
            {
                Data.ytA_Numics[i - 337] = dds.Tables["myTableName"].Rows[i][2].ToString();
                Data.ytA[i - 337] = i.ToString();
                if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                {
                    Data.ytA_tooltips[i - 337] = "null";
                }
                else
                {
                    Data.ytA_tooltips[i - 337] = dds.Tables["myTableName"].Rows[i][1].ToString();
                }
            }
            for (int i = 447; i < 523; i++)
            {
                Data.snmA_Numics[i - 447] = dds.Tables["myTableName"].Rows[i][2].ToString();
                Data.snmA[i - 447] = i.ToString();
                if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                {
                    Data.snmA_tooltips[i - 447] = "null";
                }
                else
                {
                    Data.snmA_tooltips[i - 447] = dds.Tables["myTableName"].Rows[i][1].ToString();
                }
            }
            for (int i = 523; i < 598; i++)
            {
                Data.snmB_Numics[i - 523] = dds.Tables["myTableName"].Rows[i][2].ToString();
                Data.snmB[i - 523] = i.ToString();
                if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                {
                    Data.snmB_tooltips[i - 523] = "null";
                }
                else
                {
                    Data.snmB_tooltips[i - 523] = dds.Tables["myTableName"].Rows[i][1].ToString();
                }
            }
            for (int i = 0; i < 83; i++)
            {
                Data.webrep[i] = i.ToString();
            }
            for (int i = 0; i < 30; i++)
            {
                Data.webrep2[i] = i.ToString();
            }
                //for (int i = 494; i < 638; i++)
                //{
                //    Data.kilnB[i - 494] = dds.Tables["myTableName"].Rows[i][2].ToString();
                //    if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                //    {
                //        Data.kilnB_tooltips[i - 494] = "null";
                //    }
                //    else
                //    {
                //        Data.kilnB_tooltips[i - 494] = dds.Tables["myTableName"].Rows[i][1].ToString();
                //    }
                //}
                //for (int i = 638; i < 729; i++)
                //{
                //    Data.ytB[i - 638] = dds.Tables["myTableName"].Rows[i][2].ToString();
                //    if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                //    {
                //        Data.ytB_tooltips[i - 638] = "null";
                //    }
                //    else
                //    {
                //        Data.ytB_tooltips[i - 638] = dds.Tables["myTableName"].Rows[i][1].ToString();
                //    }
                //}
                for (int i = 612; i < 676; i++)
                {
                    Data.coal_Numics[i - 612] = dds.Tables["myTableName"].Rows[i][2].ToString();
                    if (dds.Tables["myTableName"].Rows[i][1].ToString() == "")
                    {
                        Data.coal_tooltips[i - 612] = "null";
                    }
                    else
                    {
                        Data.coal_tooltips[i - 612] = dds.Tables["myTableName"].Rows[i][1].ToString();
                    }
                }
        }
    }

}