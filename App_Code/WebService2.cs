﻿using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;


/// <summary>
/// WebService2 的摘要说明
/// </summary>
//[WebService(Namespace = "http://tempuri.org/")]
//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WebService2 : System.Web.Services.WebService {

    public WebService2 () {

        //如果使用设计的组件，请取消注释以下行 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    /// <summary>
    /// 返回2#窑产量
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string[] GetAlarmData()
    {
        string[] AlarmData = new string[9];
        AlarmData[0] = Data.ytB[30];
        AlarmData[1] = Data.kilnB[2];
        AlarmData[2] = Data.kilnB[35];
        AlarmData[3] = Data.kilnB[1];
        AlarmData[4] = Data.rawB[9];
        AlarmData[5] = Data.kilnB[112];
        AlarmData[6] = "0";
        AlarmData[7] = Data.rawB[72];
        AlarmData[8] = Data.rawB[41];
        return AlarmData;
    }

    [WebMethod]
    public void SetData(string[] OpcData)
    {
        Data.ks[0] = OpcData[0];
        Data.ks[1] = OpcData[1];
        Data.ks[2] = OpcData[2];
        for (int i = 3; i < 5; i++)
        {
            Data.ks[i + 11] = OpcData[i];
        }
        for (int i = 5; i < 40; i++)
        {
            Data.rawB[i + 6] = OpcData[i];
        }
        for (int i = 40; i < 90; i++)
        {
            Data.rawB[i +28] = OpcData[i];
        }
        for (int i = 90; i < 133; i++)
        {
            Data.kilnB[i -90] = OpcData[i];
        }
        for (int i = 133; i < 233; i++)
        {
            Data.kilnB[i - 89] = OpcData[i];
        }
        for (int i = 233; i < 324; i++)
        {
            Data.ytB[i - 233] = OpcData[i];
        }
        for (int i = 324; i < 337; i++)
        {
            Data.coal[i - 306] = OpcData[i];
        }
        for (int i = 337; i < 353; i++)
        {
            Data.coal[i - 287] = OpcData[i];
        }
        //for (int i = 0; i < 25; i++)
        //{
        //    Data.ks[i] = OpcData[i];
        //}
        //for (int i = 25; i <143; i++)
        //{
        //    Data.raw[i-25] = OpcData[i];
        //}
        //for (int i = 143; i < 287; i++)
        //{
        //    Data.kiln[i-143] = OpcData[i];
        //}
        //for (int i = 287; i < 378; i++)
        //{
        //    Data.yt[i-287] = OpcData[i];
        //}
        //for (int i = 378; i < 504; i++)
        //{
        //    Data.raw2[i-378] = OpcData[i];
        //}
        //for (int i = 504; i < 630; i++)
        //{
        //    Data.kiln2[i -504] = OpcData[i];
        //}
        //for (int i = 630; i < 728; i++)
        //{
        //    Data.yt2[i - 630] = OpcData[i];
        //}
        //for (int i = 728; i < 873; i++)
        //{
        //    Data.coal[i - 728] = OpcData[i];
        //}

    }

}

