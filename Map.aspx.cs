﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Map : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Button101.Text = Membership.GetNumberOfUsersOnline().ToString();//获取目前在线人数
        if (Request.UrlReferrer != null)
        {
            Response.Write("you are from:"+Request.UrlReferrer.ToString());
        }
    }
}
