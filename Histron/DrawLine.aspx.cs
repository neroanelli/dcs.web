﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using C1.Web.C1WebChart;
//using C1.Web.C1WebChartBase;
//using C1.Web.C1WebChart3D;
//using C1.Win.C1Chart;
using System.Drawing;
using Dundas.Charting.WebControl;
public partial class _Default : System.Web.UI.Page 
{
    DataSet ds;//可被整个实例用的数据集
    string QueryName;
    #region 初始化
    /// <summary>
    /// 初始化
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //DropDownList_TN.Text = Request.QueryString["TN"];//从跳转页面获得查询参数TAG_NAME
        //QueryName = Request.QueryString["TN"];
        //DropDownList_TN.Text = QueryName;
        Chart1.ChartAreas["Default"].CursorX.UserEnabled = true ;//用户可以放大
        Chart1.ChartAreas["Default"].AxisX.View.MinSize = 5;//放大最大一屏要显示5个数据
        Chart1.Series[0].ToolTip = "#VALX---#VALY";
        //DP_ST_Year.Text = DateTime.Now.Year.ToString();
        //DP_ST_Month.Text = DateTime.Now.Month.ToString();
        //// DP_ST_Day.Text = DateTime.Now.Day.ToString();
        //DP_ST_Day.SelectedIndex = Convert.ToInt32(DateTime.Now.Day.ToString()) - 1;
        //DP_ST_Day.Text = DateTime.Now.Day.ToString();
        //DP_ST_Hour.Text = DateTime.Now.Hour.ToString();
        //DP_ST_Minute.Text = Convert.ToString(DateTime.Now.Minute - 30);
        //DP_ST_Second.Text = DateTime.Now.Second.ToString();
        //ds = Data.GetHisData("RAC4211/PV1", "2008-02-25 09:50:00", "2008-02-25 10:00:00", "00:00:50");//默认的访问数据集
        if (!IsPostBack)
        {
            //DropDownList_TN.Text = Request.QueryString["TN"];//从跳转页面获得查询参数TAG_NAME
            QueryName = Request.QueryString["TN"];
            DropDownList_TN.Text = QueryName;
            DP_ST_Year.Text = DateTime.Now.Year.ToString();
            //DP_ST_Month.Text = DateTime.Now.Month.ToString();
            DP_ST_Month.SelectedIndex =Convert.ToInt32( DateTime.Now.Month.ToString())-1;
           // DP_ST_Day.Text = DateTime.Now.Day.ToString();
            DP_ST_Day.SelectedIndex  =Convert.ToInt32 ( DateTime.Now.Day.ToString())-1;
            DP_ST_Day.Text = DateTime.Now.Day.ToString();
            DP_ST_Hour.SelectedIndex = Convert.ToInt32(DateTime.Now.Hour.ToString()) - 2;
            //DP_ST_Minute.Text = Convert.ToString(DateTime.Now.Minute-30);
            DP_ST_Minute.Text = Convert.ToString(DateTime.Now.Minute );
            DP_ST_Second.Text = DateTime.Now.Second.ToString();
            DP_END_Year.Text = DateTime.Now.Year.ToString();
            DP_END_Month.SelectedIndex = Convert.ToInt32(DateTime.Now.Month.ToString()) - 1;
            //DP_END_Day.Text = DateTime.Now.Day.ToString();
            DP_END_Day.SelectedIndex = Convert.ToInt32(DateTime.Now.Day.ToString()) - 1;
            DP_END_Day.Text = DateTime.Now.Day.ToString();
            DP_END_Hour.SelectedIndex = Convert.ToInt32(DateTime.Now.Hour.ToString());
            DP_END_Minute.Text = DateTime.Now.Minute.ToString();
            DP_END_Second.Text = DateTime.Now.Second.ToString();
            CheckBox1.Checked = false;
            DrawTrend_Init();
        }
    }
    #endregion

    protected void Button1_Click(object sender, EventArgs e)
    {
        //DrawTrend_old();//调用显示函数 
        DrawTrend_Auto();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        //Chart1.ChartAreas["Default"].CursorX.UserEnabled = false;
        //Response.Write("<script>window.open('HisData.aspx?TN=CONCH_A')</script>");
        //TextBox_TagN.Text = Request.QueryString["TN"];

    }
    # region 计算查询数据量 (结束时间-开始时间)/interval 返回false 则提示查询量太大，true则进行查询
    private bool ColateNum()  //返回false 则提示查询量太大，true则进行查询
    {
        int year = Convert.ToInt32(DP_END_Year.SelectedValue.ToString()) - Convert.ToInt32(DP_ST_Year.SelectedValue.ToString());
        int month = Convert.ToInt32(DP_END_Month.SelectedValue.ToString()) - Convert.ToInt32(DP_ST_Month.SelectedValue.ToString());
        int day = Convert.ToInt32(DP_END_Day.SelectedValue.ToString()) - Convert.ToInt32(DP_ST_Day.SelectedValue.ToString());
        int hour = Convert.ToInt32(DP_END_Hour.SelectedValue.ToString()) - Convert.ToInt32(DP_ST_Hour.SelectedValue.ToString());
        int minute = Convert.ToInt32(DP_END_Minute.SelectedValue.ToString()) - Convert.ToInt32(DP_ST_Minute.SelectedValue.ToString());
        int Count = (month * 2592000) + (day * 86400) + (hour * 3600) + (minute * 60);
        int Num = Count / CountInterval(DropDownList_interv.SelectedValue.ToString());
        if (year >= 1)  //超过1年 不予查询
        {
            Response.Write("<script>alert('您选择的时间太久，都超过1年了！');</script>");
            return false;
        }
        else
        {
            if (Num > 700)
            {
                Response.Write("<script>alert('您查询的量太大了！可能是时间间隔选择不合理，为了服务器的安全请适当放大间隔！');</script>");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
#endregion

    # region 计算返回interval的秒数
//    int caseSwitch = 1;
//switch (caseSwitch)
//{
//    case 1: 
//        Console.WriteLine("Case 1");
//        break;
//    case 2:
//        Console.WriteLine("Case 2");
//        break;
//    default:
//        Console.WriteLine("Default case");
//        break;
//}

    private int CountInterval(string interval)
    {
        int Ret=0;//返回值
        switch (interval)
        {
            case "00:00:01":
                Ret = 1;
                return Ret;
                break;
            case "00:00:05":
                Ret = 5;
                return Ret;
                break;
            case "00:00:10":
                Ret = 10;
                return Ret;
                break;
            case "00:00:20":
                Ret = 20;
                return Ret;
                break;
            case "00:00:30":
                Ret = 30;
                return Ret;
                break;
            case "00:01:00":
                Ret = 60;
                return Ret;
                break;
            case "00:02:00":
                Ret = 120;
                return Ret;
                break;
            case "00:05:00":
                Ret =300;
                return Ret;
                break;
            case "00:10:00":
                Ret = 600;
                return Ret;
                break;
            case "00:15:00":
                Ret = 900;
                return Ret;
                break;
            case "00:20:00":
                Ret = 1200;
                return Ret;
                break;
            case "00:30:00":
                Ret = 1800;
                return Ret;
                break;
            case "00:59:00":
                Ret = 3600;
                return Ret;
                break;
            default :
                Ret=0;
                return Ret;
                break;
        }
        return Ret;
    }
#endregion
    private void DrawTrend()
    {
        Response.Write("<script>alert('趋势功能正在建设中，请等待，谢谢！');</script>");
    }

    #region 显示函数
    /// <summary>
    /// 显示函数
    /// </summary>
    private void DrawTrend_old()
    {
        int con =Convert.ToInt32(Application["Con"]);
        Chart1.Series[0].Points.Clear();
        string interval = DropDownList_interv.SelectedValue.ToString();
        string tagname = DropDownList_TN.Text.ToString();
        string starttime = Data.GetTime(DP_ST_Year.SelectedValue.ToString(),DP_ST_Month.SelectedValue.ToString(),DP_ST_Day.SelectedValue.ToString(),DP_ST_Hour.SelectedValue.ToString(),DP_ST_Minute.SelectedValue.ToString(),DP_ST_Second.SelectedValue.ToString());
        string endtime = Data.GetTime(DP_END_Year.SelectedValue.ToString(),DP_END_Month.SelectedValue.ToString(),DP_END_Day.SelectedValue.ToString(),DP_END_Hour.SelectedValue.ToString(),DP_END_Minute.SelectedValue.ToString(),DP_END_Second.SelectedValue.ToString());
        if (string.Compare(starttime, endtime) >= 0)// starttime>=endtime  开始时间不比结束时间早
        {
           Response.Write("<script>alert('您选择的开始时间比结束时间还要迟哦！');</script>");
           return;
        }
        else
        {
            if (Convert.ToInt32(DP_END_Year.SelectedValue.ToString()) - Convert.ToInt32(DP_ST_Year.SelectedValue.ToString()) >= 1)  //超过1年 不予查询
            {
                Response.Write("<script>alert('您选择的时间太久，都超过1年了！');</script>");
            }
            if (con == 1 && ColateNum() == true)//初始为1 可以访问历史数据库 并且计算查询数据量可以访问数据库
            {
                Application["Con"] = 0;//加锁
                ds = Data.GetHisData(tagname, starttime, endtime, interval);
                DataView DV = new DataView(ds.Tables[0]);
                if (CheckBox1.Checked == true)//选择了Zoom Enable
                {
                    Chart1.Series[0].Points.DataBindY(DV, "VALUE");
                    Chart1.ChartAreas["Default"].CursorX.UserEnabled = true;
                }
                else //没有选择Zoom Enable
                {
                    Chart1.Series[0].Points.DataBindXY(DV, "DATETIME", DV, "VALUE");
                    Chart1.ChartAreas["Default"].CursorX.UserEnabled = false;
                }
                Chart1.ChartAreas["Default"].AxisX.View.Position = 10.0;
                Chart1.Series[0]["EmptyPointValue"] = "average";
                //Chart1.ChartAreas["Default"].CursorX.UserEnabled = true;
                Chart1.ChartAreas["Default"].AxisX.View.MinSize = 5;
                //ds.Dispose();
                DV.Dispose();
                ds.Dispose();
                Application["Con"] = 1;//释放锁    
            }
            //ds = Data.GetHisData(tagname, "2007-11-14 16:00:00", "2007-11-14 16:30:00", interval);
            //ds = Data.GetHisData(tagname,starttime,endtime,interval);

            //Chart1.Series[0].Points.Clear();
            //Chart1.Series[0].Points.AddXY(3, 6);
            //Chart1.Series[0].Points.AddXY(4, 7);
            //Chart1.Series[0].Points.AddXY(5, 2);

            //Chart1.Series[0].Points.AddXY(6, 6);
            //Chart1.Series[0].Points.AddXY(7, 7);
            //Chart1.Series[0].Points.AddXY(8, 2);

            //Chart1.Series[0].Points.AddXY(9, 6);
            //Chart1.Series[0].Points.AddXY(10, 7);
            //Chart1.Series[0].Points.AddXY(11, 2);

            //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //{
            //    if (ds.Tables[0].Rows[i]["VALUE"].ToString() == "" || ds.Tables[0].Rows[i]["VALUE"].Equals("????"))
            //    {
            //        Chart1.Series[0].Points.AddXY( ds.Tables[0].Rows[i]["DATETIME"],"0");
            //    }
            //    else
            //    {
            //        Chart1.Series[0].Points.AddXY(ds.Tables[0].Rows[i]["DATETIME"], ds.Tables[0].Rows[i]["VALUE"]);
            //    }
            //}
            
            else
            {
                Response.Write("<script>alert('对不起！可能别的用户在查询大量数据，服务器有点忙，或者该时间段没有数据，请换个时间再查询好吗？或者您查询的量太大了！可能是时间间隔选择不合理，为了服务器的安全请适当放大间隔！');</script>");
                return;
            }
        }
    }
    #endregion

    #region 自适应显示函数
    private void DrawTrend_Auto()
    {
        DateTime StartTime, EndTime;
        StartTime = DateTime.Now;
        //DropDownList_interv.Visible = false;
        int con = Convert.ToInt32(Application["Con"]);
        Chart1.Series[0].Points.Clear();
        //string interval = DropDownList_interv.SelectedValue.ToString();
        //string tagname = DropDownList_TN.SelectedValue.ToString();
        string tagname = DropDownList_TN.Text;//QueryName;
        string starttime = Data.GetTime(DP_ST_Year.SelectedValue.ToString(), DP_ST_Month.SelectedValue.ToString(), DP_ST_Day.SelectedValue.ToString(), DP_ST_Hour.SelectedValue.ToString(), DP_ST_Minute.SelectedValue.ToString(), DP_ST_Second.SelectedValue.ToString());
        string endtime = Data.GetTime(DP_END_Year.SelectedValue.ToString(), DP_END_Month.SelectedValue.ToString(), DP_END_Day.SelectedValue.ToString(), DP_END_Hour.SelectedValue.ToString(), DP_END_Minute.SelectedValue.ToString(), DP_END_Second.SelectedValue.ToString());
        if (string.Compare(starttime, endtime) >= 0)// starttime>=endtime  开始时间不比结束时间早
        {
            Response.Write("<script>alert('您选择的开始时间比结束时间还要迟哦！');</script>");
            return;
        }
        else
        {
            if (Convert.ToInt32(DP_END_Year.SelectedValue.ToString()) - Convert.ToInt32(DP_ST_Year.SelectedValue.ToString()) >= 1)  //超过1年 不予查询
            {
                Response.Write("<script>alert('您选择的时间太久，都超过1年了！');</script>");
            }
            if (con == 1)//初始为1 可以访问历史数据库 并且计算查询数据量可以访问数据库
            {
                string interval = Data.GetInterval(DP_ST_Month.SelectedValue.ToString(), DP_ST_Day.SelectedValue.ToString(), DP_ST_Hour.SelectedValue.ToString(), DP_ST_Minute.SelectedValue.ToString(), DP_ST_Second.SelectedValue.ToString(), DP_END_Month.SelectedValue.ToString(), DP_END_Day.SelectedValue.ToString(), DP_END_Hour.SelectedValue.ToString(), DP_END_Minute.SelectedValue.ToString(), DP_END_Second.SelectedValue.ToString());
                Application["Con"] = 0;//加锁
                ds = Data.GetHisData(tagname, starttime, endtime, interval);
                DataView DV = new DataView(ds.Tables[0]);
                Application["Con"] = 1;//释放锁 
                if (CheckBox1.Checked == true)//选择了Zoom Enable
                {
                    Chart1.Series[0].Points.DataBindY(DV, "VALUE");
                    Chart1.ChartAreas["Default"].CursorX.UserEnabled = true;
                }
                else //没有选择Zoom Enable
                {
                    Chart1.Series[0].Points.DataBindXY(DV, "DATETIME", DV, "VALUE");
                    Chart1.ChartAreas["Default"].CursorX.UserEnabled = false;
                }
                Chart1.ChartAreas["Default"].AxisX.View.Position = 10.0;
                Chart1.Series[0]["EmptyPointValue"] = "average";
                //Chart1.ChartAreas["Default"].CursorX.UserEnabled = true;
                Chart1.ChartAreas["Default"].AxisX.View.MinSize = 5;
                //ds.Dispose();
                DV.Dispose();
                ds.Dispose();
                //Application["Con"] = 1;//释放锁 
            }
            //ds = Data.GetHisData(tagname, "2007-11-14 16:00:00", "2007-11-14 16:30:00", interval);
            //ds = Data.GetHisData(tagname,starttime,endtime,interval);

            //Chart1.Series[0].Points.Clear();
            //Chart1.Series[0].Points.AddXY(3, 6);
            //Chart1.Series[0].Points.AddXY(4, 7);
            //Chart1.Series[0].Points.AddXY(5, 2);

            //Chart1.Series[0].Points.AddXY(6, 6);
            //Chart1.Series[0].Points.AddXY(7, 7);
            //Chart1.Series[0].Points.AddXY(8, 2);

            //Chart1.Series[0].Points.AddXY(9, 6);
            //Chart1.Series[0].Points.AddXY(10, 7);
            //Chart1.Series[0].Points.AddXY(11, 2);

            //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //{
            //    if (ds.Tables[0].Rows[i]["VALUE"].ToString() == "" || ds.Tables[0].Rows[i]["VALUE"].Equals("????"))
            //    {
            //        Chart1.Series[0].Points.AddXY( ds.Tables[0].Rows[i]["DATETIME"],"0");
            //    }
            //    else
            //    {
            //        Chart1.Series[0].Points.AddXY(ds.Tables[0].Rows[i]["DATETIME"], ds.Tables[0].Rows[i]["VALUE"]);
            //    }
            //}

            else
            {
                Response.Write("<script>alert('对不起！可能别的用户在查询大量数据，服务器有点忙，或者该时间段没有数据，请换个时间再查询好吗？或者您查询的量太大了！可能是时间间隔选择不合理，为了服务器的安全请适当放大间隔！');</script>");
                return;
            }
        }
        //DropDownList_interv.Visible = true;
        EndTime = DateTime.Now;
        TimeSpan Sp = EndTime - StartTime;
        Label2.Text = "We used " + Sp.Seconds.ToString() + "Second for checking";
    }
    #endregion 

#region 
    private void DrawTrend_Init()
    {
        DateTime StartTime, EndTime;
        StartTime = DateTime.Now;
        int con;// = Convert.ToInt32(Application["Con"]);
        Chart1.Series[0].Points.Clear();
        //string interval = DropDownList_interv.SelectedValue.ToString();
        string tagname = DropDownList_TN.Text.ToString();   // RAC4211/PV1
        string month = null; string day = null; string SThour = null; string ENDhour = null; string minute = null; string second = null;
        if (Convert.ToInt32(DateTime.Now.Month.ToString()) < 10)
        {
            month = "0" + DateTime.Now.Month.ToString();
        }
        else
        {
            month = DateTime.Now.Month.ToString();
        }
        if (Convert.ToInt32(DateTime.Now.Day.ToString()) < 10)
        {
            day = "0" + DateTime.Now.Day.ToString();
        }
        else
        {
            day = DateTime.Now.Day.ToString();
        }
        if (Convert.ToInt32(DateTime.Now.Hour.ToString()) < 10)
        {
            ENDhour = "0" + DateTime.Now.Hour.ToString();
            SThour = "0" + Convert.ToString(Convert.ToInt32(DateTime.Now.Hour.ToString())-2);
        }
        if (Convert.ToInt32(DateTime.Now.Hour.ToString()) == 10)
        {
            ENDhour = DateTime.Now.Hour.ToString();
            SThour = "0" + Convert.ToString(Convert.ToInt32(DateTime.Now.Hour.ToString()) - 2);
        }
        if (Convert.ToInt32(DateTime.Now.Hour.ToString()) ==11)
        {
            ENDhour = DateTime.Now.Hour.ToString();
            SThour = "0" + Convert.ToString(Convert.ToInt32(DateTime.Now.Hour.ToString()) - 2);
        }
        if (Convert.ToInt32(DateTime.Now.Hour.ToString()) >=12)
        {
            ENDhour =  DateTime.Now.Hour.ToString();
            SThour =  Convert.ToString(Convert.ToInt32(DateTime.Now.Hour.ToString()) - 2);
        }
        if (Convert.ToInt32(DateTime.Now.Minute.ToString()) < 10)
        {
            minute = "0" + DateTime.Now.Minute.ToString();
        }
        else
        {
            minute =  DateTime.Now.Minute.ToString();
        }
        if (Convert.ToInt32(DateTime.Now.Second.ToString()) < 10)
        {
            second = "0" + DateTime.Now.Second.ToString();
        }
        else
        {
            second  = DateTime.Now.Second.ToString();
        }
        //string tagname = "RAC4211/PV1";
        //string starttime = Data.GetTime(DP_ST_Year.SelectedValue.ToString(), DP_ST_Month.SelectedValue.ToString(), DP_ST_Day.SelectedValue.ToString(), DP_ST_Hour.SelectedValue.ToString(), DP_ST_Minute.SelectedValue.ToString(), DP_ST_Second.SelectedValue.ToString());
        //if (Convert.ToInt32(DateTime.Now.Hour.ToString()) >= 2)//现在时间 小时>2
        //{
        //    string starttime = Data.GetTime(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), Convert.ToString(Convert.ToInt32(DateTime.Now.Hour.ToString()) - 2), DateTime.Now.Minute.ToString(), DateTime.Now.Second.ToString());
        //}
        //else //目前没有做处理，因为每日0点，1点 通常没有人查询趋势
        //{
        //    string starttime = Data.GetTime(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), Convert.ToString(Convert.ToInt32(DateTime.Now.Hour.ToString()) - 2), DateTime.Now.Minute.ToString(), DateTime.Now.Second.ToString());
        //}
        string starttime = Data.GetTime(DateTime.Now.Year.ToString(), month, day, SThour, minute, second);
            //string endtime = Data.GetTime(DP_END_Year.SelectedValue.ToString(), DP_END_Month.SelectedValue.ToString(), DP_END_Day.SelectedValue.ToString(), DP_END_Hour.SelectedValue.ToString(), DP_END_Minute.SelectedValue.ToString(), DP_END_Second.SelectedValue.ToString());
        string endtime = Data.GetTime(DateTime.Now.Year.ToString(), month, day, ENDhour, minute, second);
        try
        {
            con = Convert.ToInt32(Application["Con"]);
            if (con == 1)//初始为1 可以访问历史数据库 并且计算查询数据量可以访问数据库
            {
                Application["Con"] = 0;//加锁
                string interval = Data.GetInterval(DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), Convert.ToString(Convert.ToInt32(DateTime.Now.Hour.ToString()) - 2), DateTime.Now.Minute.ToString(), DateTime.Now.Second.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), DateTime.Now.Hour.ToString(), DateTime.Now.Minute.ToString(), DateTime.Now.Second.ToString());
                ds = Data.GetHisData(tagname, starttime, endtime, interval);//"2008-02-25 09:50:00", "2008-02-25 10:00:00", "00:00:50"
                //dds = Data.GetHisData("RAC4211/PV1", "2008-3-07 09:50:00", "2008-03-07 11:50:00", "00:00:11");
                DataView DV = new DataView(ds.Tables[0]);
                Application["Con"] = 1;//释放锁 
                if (CheckBox1.Checked == true)//选择了Zoom Enable
                {
                    Chart1.Series[0].Points.DataBindY(DV, "VALUE");
                    Chart1.ChartAreas["Default"].CursorX.UserEnabled = true;
                }
                else //没有选择Zoom Enable
                {
                    Chart1.Series[0].Points.DataBindXY(DV, "DATETIME", DV, "VALUE");
                    Chart1.ChartAreas["Default"].CursorX.UserEnabled = false;
                }
                Chart1.ChartAreas["Default"].AxisX.View.Position = 10.0;
                Chart1.Series[0]["EmptyPointValue"] = "average";
                //Chart1.ChartAreas["Default"].CursorX.UserEnabled = true;
                Chart1.ChartAreas["Default"].AxisX.View.MinSize = 5;
                //ds.Dispose();
                DV.Dispose();
                ds.Dispose();
                //Application["Con"] = 1;//释放锁 
            }
            else
            {
                Response.Write("<script>alert('对不起！可能别的用户在查询大量数据，服务器有点忙，或者该时间段没有数据，请换个时间再查询好吗？或者您查询的量太大了！可能是时间间隔选择不合理，为了服务器的安全请适当放大间隔！');</script>");
                return;
            }
        }
        catch(Exception ex)
        {
            Response.Write(ex.ToString());
        }
        //DropDownList_interv.Visible = true;
        EndTime = DateTime.Now;
        TimeSpan Sp = EndTime - StartTime;
        Label2.Text = "We used " + Sp.Seconds.ToString()+"Second for checking";
    }
#endregion

    protected void DropDownList_interv_SelectedIndexChanged(object sender, EventArgs e)
    {
        DrawTrend_old();//调用显示函数
    }
    protected void DropDownList_TN_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DrawTrend_old();//调用显示函数
        DrawTrend_Auto();
    }
    //protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    //{
    //    DrawTrend();//调用显示函数 
    //}

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {

    }
}
// DateTime   a   =   DateTime.Parse(A);
//Button1.Text   =   string.Compare("2006   06   09",   "2006   06   10").ToString();
