﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="DrawLine.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<%@ Register Assembly="DundasWebChart" Namespace="Dundas.Charting.WebControl" TagPrefix="DCWC" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>历史趋势</title>
    <script> 
   function window.onload() 
   { 
   var obj=document.getElementById("Chart1"); 
   window.resizeTo(obj.offsetWidth +10,obj.offsetHeight +80); 
   window.scroll=false;
   } 
   </script>
</head>
<body style="background-color: #ccccff">
    <form id="form1" runat="server">
    
   
        <DCWC:Chart ID="Chart1" runat="server" BackColor="LightSteelBlue" BorderLineColor="LightSlateGray"
            Height="248px" Palette="GrayScale" Style="z-index: 100; left: 3px; position: absolute;
            top: 4px; background-color: transparent;" Width="423px" RepeatAnimation="True">
            <Legends>
                <DCWC:Legend Alignment="Far" BackColor="Lavender" BorderColor="Gray" Docking="Top"
                    Enabled="False" Name="Default" ShadowOffset="2">
                    <Position Height="6.088993" Width="51.7894745" X="40.4736824" Y="14.93589" />
                </DCWC:Legend>
            </Legends>
            <BorderSkin FrameBackColor="SteelBlue" FrameBackGradientEndColor="LightBlue" PageColor="AliceBlue" />
            <Series>
                <DCWC:Series BorderColor="64, 64, 64" ChartType="Line" CustomAttributes="LabelStyle=Center, RoseDrawingStyle=Area, RoseSectorAngle=45"
                    Name="Series1" ShadowOffset="2" YValueType="Double">
                    <SmartLabels Enabled="True" />
                    <Points>
                        <DCWC:DataPoint BorderColor="64, 64, 64" Empty="True" MarkerStyle="Circle" YValues="2" />
                        <DCWC:DataPoint BorderColor="64, 64, 64" MarkerStyle="Circle" XValue="1" YValues="5" />
                    </Points>
                </DCWC:Series>
                <DCWC:Series BorderColor="64, 64, 64" ChartType="Line" CustomAttributes="LabelStyle=Center, RoseDrawingStyle=Area, RoseSectorAngle=45"
                    Name="Series2" ShadowOffset="2" XValueType="DateTime">
                    <SmartLabels Enabled="True" />
                </DCWC:Series>
                <DCWC:Series BorderColor="64, 64, 64" ChartType="Line" CustomAttributes="RoseDrawingStyle=Area, RoseSectorAngle=45"
                    Name="Series5" ShadowOffset="2" XValueType="DateTime">
                    <SmartLabels Enabled="True" />
                </DCWC:Series>
            </Series>
            <UI>
                <Toolbar>
                    <BorderSkin PageColor="Transparent" SkinStyle="Emboss" />
                    <Items>
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                    </Items>
                </Toolbar>
                <ContextMenu>
                    <Items>
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                        <DCWC:CommandUIItem CommandName="Separator" />
                    </Items>
                </ContextMenu>
                <Commands>
                    <DCWC:Command CommandType="LoadGroup">
                        <SubCommands>
                            <DCWC:Command CommandType="Separator">
                            </DCWC:Command>
                        </SubCommands>
                    </DCWC:Command>
                </Commands>
            </UI>
            <ChartAreas>
                <DCWC:ChartArea BackColor="Lavender" BorderColor="" Name="Default" ShadowOffset="2">
                    <AxisX>
                        <LabelStyle Format="g" />
                        <MajorTickMark Style="cross" />
                        <MajorGrid LineColor="LightSteelBlue" LineStyle="Dash" />
                        <MinorTickMark Size="2" />
                    </AxisX>
                    <Area3DStyle Light="Realistic" WallWidth="0" />
                    <InnerPlotPosition Height="77.676" Width="85.53394" X="14.4660568" Y="3.10166" />
                    <Position Height="67.7057953" Width="84.52631" X="7.73683929" Y="24.0248833" />
                    <AxisY StartFromZero="False">
                        <MajorGrid LineColor="LightSteelBlue" LineStyle="Dash" />
                        <MinorTickMark Size="2" />
                    </AxisY>
                </DCWC:ChartArea>
            </ChartAreas>
            <Titles>
                <DCWC:Title Name="Title1">
                </DCWC:Title>
            </Titles>
        </DCWC:Chart>
        <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" Style="left: 320px;
            position: relative; top: 234px" Text="Zoom Enable" OnCheckedChanged="CheckBox1_CheckedChanged" Font-Size="X-Small" />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Style="z-index: 101;
            left: 13px; position: absolute; top: 229px" Text="获取趋势" BackColor="Transparent" Font-Size="Small" ForeColor="Red" BorderColor="Red" Height="18px" />
                &nbsp;
        <asp:DropDownList ID="DropDownList_interv" runat="server" Style="z-index: 103; left: 362px;
            position: absolute; top: 7px" ForeColor="#0000C0" AutoPostBack="True" BackColor="LightSteelBlue" OnSelectedIndexChanged="DropDownList_interv_SelectedIndexChanged" Width="55px" Visible="False" Height="12px">
            <asp:ListItem Selected="True" Value="00:00:01">1秒</asp:ListItem>
            <asp:ListItem Value="00:00:05">5秒</asp:ListItem>
            <asp:ListItem Value="00:00:10">10秒</asp:ListItem>
            <asp:ListItem Value="00:00:20">20秒</asp:ListItem>
            <asp:ListItem Value="00:00:30">30秒</asp:ListItem>
            <asp:ListItem Value="00:01:25">1分钟</asp:ListItem>
            <asp:ListItem Value="00:02:00">2分钟</asp:ListItem>
            <asp:ListItem Value="00:05:00">5分钟</asp:ListItem>
            <asp:ListItem Value="00:10:00">10分钟</asp:ListItem>
            <asp:ListItem Value="00:15:00">15分钟</asp:ListItem>
            <asp:ListItem Value="00:20:00">20分钟</asp:ListItem>
            <asp:ListItem Value="00:30:00">30分钟</asp:ListItem>
            <asp:ListItem Value="00:59:00">1小时</asp:ListItem>
        </asp:DropDownList>
        <asp:Label ID="Label1" runat="server" Style="z-index: 104; left: 239px; position: absolute;
            top: 10px" Text="请选取时间间隔" ForeColor="Red" Width="122px" Visible="False" Font-Size="Smaller"></asp:Label>
        <asp:Label ID="Label4" runat="server" ForeColor="Red" Style="z-index: 105; left: 4px;
            position: absolute; top: 27px" Text="请选取开始时间" Width="119px" Font-Size="Small"></asp:Label>
        <asp:Label ID="Label5" runat="server" ForeColor="Red" Style="z-index: 106; left: 4px;
            position: absolute; top: 48px" Text="请选取结束时间" Width="118px" Font-Size="Small"></asp:Label>
        <asp:Label ID="Label2" runat="server" Height="17px" Style="z-index: 107; left: 7px;
            position: absolute; top: 252px" Text="目前系统只从2008-02-25 10:30:00开始记录数据，请大家谨慎选取开始点" Width="323px" ForeColor="Red" Font-Size="X-Small"></asp:Label>
        &nbsp;
        <asp:Label ID="Label3" runat="server" Style="z-index: 108; left: 5px; position: absolute;
            top: 9px" Text="请选取采集点" ForeColor="Red" Width="96px" Font-Size="Small" Height="10px"></asp:Label>
        <asp:Label ID="DropDownList_TN" runat="server" Font-Size="Small" ForeColor="Red" Height="10px"
            Style="z-index: 108; left: 134px; position: absolute; top: 9px" Text="请选取采集点"
            Width="96px"></asp:Label>
        <asp:DropDownList ID="DropDownList_TTN" runat="server" Style="z-index: 109; left: 101px;
            position: absolute; top: 6px" ForeColor="Blue" AutoPostBack="True" BackColor="LightSteelBlue" OnSelectedIndexChanged="DropDownList_TN_SelectedIndexChanged" Font-Size="XX-Small" Height="6px" Visible="False">
            <asp:ListItem>TIA11103</asp:ListItem>
            <asp:ListItem>WIA16101</asp:ListItem>
            <asp:ListItem>RAC4211/PV5</asp:ListItem>
            <asp:ListItem>42141AI</asp:ListItem>
            <asp:ListItem>RAC4211/PV1</asp:ListItem>
            <asp:ListItem>TI15141</asp:ListItem>
            <asp:ListItem>L15141</asp:ListItem>
            <asp:ListItem>XI15141</asp:ListItem>
            <asp:ListItem>II15141</asp:ListItem>
            <asp:ListItem>II12130</asp:ListItem>
            <asp:ListItem>II11150</asp:ListItem>
            <asp:ListItem>LICA11128</asp:ListItem>
            <asp:ListItem>BCF11131</asp:ListItem>
            <asp:ListItem>BCL12101</asp:ListItem>
            <asp:ListItem>PIA12109</asp:ListItem>
            <asp:ListItem>PIA11129</asp:ListItem>
            <asp:ListItem>FIA15120</asp:ListItem>
            <asp:ListItem>ZI12124MV</asp:ListItem>
            <asp:ListItem>ZI11112MV</asp:ListItem>
            <asp:ListItem>ZI11114MV</asp:ListItem>
            <asp:ListItem>ZI12110MV</asp:ListItem>
            <asp:ListItem>TI15142</asp:ListItem>
            <asp:ListItem>L15142</asp:ListItem>
            <asp:ListItem>XI15142</asp:ListItem>
            <asp:ListItem>II15142</asp:ListItem>
            <asp:ListItem>ZI11121MV</asp:ListItem>
            <asp:ListItem>PIA11107</asp:ListItem>
            <asp:ListItem>PIA12108</asp:ListItem>
            <asp:ListItem>II12131</asp:ListItem>
            <asp:ListItem>II11151</asp:ListItem>
            <asp:ListItem>ZI11116MV</asp:ListItem>
            <asp:ListItem>ZI11117MV</asp:ListItem>
            <asp:ListItem>ZI12100MV</asp:ListItem>
            <asp:ListItem>PICA11101</asp:ListItem>
            <asp:ListItem>PICA12102</asp:ListItem>
            <asp:ListItem>II15150</asp:ListItem>
            <asp:ListItem>II11165</asp:ListItem>
            <asp:ListItem>ZI11115MV</asp:ListItem>
            <asp:ListItem>ZI15141MV</asp:ListItem>
            <asp:ListItem>LIA15105</asp:ListItem>
            <asp:ListItem>TIA15111</asp:ListItem>
            <asp:ListItem>TIA15112</asp:ListItem>
            <asp:ListItem>TIA15113</asp:ListItem>
            <asp:ListItem>TIA15114</asp:ListItem>
            <asp:ListItem>TI11130</asp:ListItem>
            <asp:ListItem>TI12110</asp:ListItem>
            <asp:ListItem>TIA11107</asp:ListItem>
            <asp:ListItem>II15151</asp:ListItem>
            <asp:ListItem>II11166</asp:ListItem>
            <asp:ListItem>PI10106</asp:ListItem>
            <asp:ListItem>ZI15142MV</asp:ListItem>
            <asp:ListItem>PIA15111</asp:ListItem>
            <asp:ListItem>TIA10101</asp:ListItem>
            <asp:ListItem>II15152</asp:ListItem>
            <asp:ListItem>ZI15143MV</asp:ListItem>
            <asp:ListItem>BCF15105</asp:ListItem>
            <asp:ListItem>PIA15113</asp:ListItem>
            <asp:ListItem>LIA15107</asp:ListItem>
            <asp:ListItem>AIA15103</asp:ListItem>
            <asp:ListItem>AIA15104</asp:ListItem>
            <asp:ListItem>42131AI</asp:ListItem>
            <asp:ListItem>42142AI</asp:ListItem>
            <asp:ListItem>42143AI</asp:ListItem>
            <asp:ListItem>42181AI</asp:ListItem>
            <asp:ListItem>42144AI</asp:ListItem>
            <asp:ListItem>42154AI</asp:ListItem>
            <asp:ListItem>422E3EI</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_ST_Year" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue" Style="z-index: 110; left: 101px; position: absolute; top: 24px">
            <asp:ListItem>2006</asp:ListItem>
            <asp:ListItem Selected="True">2007</asp:ListItem>
            <asp:ListItem>2008</asp:ListItem>
            <asp:ListItem>2009</asp:ListItem>
            <asp:ListItem>2010</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_END_Year" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue" Style="z-index: 111; left: 101px; position: absolute; top: 46px">
            <asp:ListItem>2006</asp:ListItem>
            <asp:ListItem Selected="True">2007</asp:ListItem>
            <asp:ListItem>2008</asp:ListItem>
            <asp:ListItem>2009</asp:ListItem>
            <asp:ListItem>2010</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_ST_Day" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue" Style="z-index: 112; left: 212px; position: absolute; top: 24px"
            Width="38px">
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem Selected="True">14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
            <asp:ListItem>24</asp:ListItem>
            <asp:ListItem>25</asp:ListItem>
            <asp:ListItem>26</asp:ListItem>
            <asp:ListItem>27</asp:ListItem>
            <asp:ListItem>28</asp:ListItem>
            <asp:ListItem>29</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem>31</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_END_Day" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue" Style="z-index: 113; left: 212px; position: absolute; top: 46px"
            Width="38px">
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem Selected="True">14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
            <asp:ListItem>24</asp:ListItem>
            <asp:ListItem>25</asp:ListItem>
            <asp:ListItem>26</asp:ListItem>
            <asp:ListItem>27</asp:ListItem>
            <asp:ListItem>28</asp:ListItem>
            <asp:ListItem>29</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem>31</asp:ListItem>
        </asp:DropDownList>
        <asp:Label ID="Label6" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 114;
            left: 149px; position: absolute; top: 27px" Text="年"></asp:Label>
        <asp:Label ID="Label12" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 115;
            left: 149px; position: absolute; top: 50px" Text="年"></asp:Label>
        <asp:DropDownList ID="DP_ST_Month" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="#0000C0" Style="z-index: 116; left: 160px; position: absolute; top: 24px"
            Width="37px">
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_END_Month" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="#0000C0" Style="z-index: 117; left: 160px; position: absolute; top: 46px"
            Width="37px">
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
        </asp:DropDownList>
        <asp:Label ID="Label7" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 118;
            left: 198px; position: absolute; top: 27px" Text="月"></asp:Label>
        <asp:Label ID="Label13" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 119;
            left: 198px; position: absolute; top: 49px" Text="月"></asp:Label>
        <asp:Label ID="Label8" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 120;
            left: 250px; position: absolute; top: 26px" Text="日"></asp:Label>
        <asp:Label ID="Label14" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 121;
            left: 250px; position: absolute; top: 49px" Text="日"></asp:Label>
        <asp:DropDownList ID="DP_ST_Hour" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue"  Style="z-index: 122; left: 262px; position: absolute;
            top: 24px">
            <asp:ListItem Value="00">0</asp:ListItem>
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem Selected="True">16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_END_Hour" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue" Style="z-index: 123; left: 261px; position: absolute;
            top: 46px">
            <asp:ListItem Value="00">0</asp:ListItem>
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem Selected="True">16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_ST_Minute" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue" Style="z-index: 124; left: 314px; position: absolute; top: 24px">
            <asp:ListItem Value="00">0</asp:ListItem>
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
            <asp:ListItem>24</asp:ListItem>
            <asp:ListItem>25</asp:ListItem>
            <asp:ListItem>26</asp:ListItem>
            <asp:ListItem>27</asp:ListItem>
            <asp:ListItem>28</asp:ListItem>
            <asp:ListItem>29</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem>31</asp:ListItem>
            <asp:ListItem>32</asp:ListItem>
            <asp:ListItem>33</asp:ListItem>
            <asp:ListItem>34</asp:ListItem>
            <asp:ListItem>35</asp:ListItem>
            <asp:ListItem>36</asp:ListItem>
            <asp:ListItem>37</asp:ListItem>
            <asp:ListItem>38</asp:ListItem>
            <asp:ListItem>39</asp:ListItem>
            <asp:ListItem>40</asp:ListItem>
            <asp:ListItem>41</asp:ListItem>
            <asp:ListItem>42</asp:ListItem>
            <asp:ListItem>43</asp:ListItem>
            <asp:ListItem>44</asp:ListItem>
            <asp:ListItem>45</asp:ListItem>
            <asp:ListItem>46</asp:ListItem>
            <asp:ListItem>47</asp:ListItem>
            <asp:ListItem>48</asp:ListItem>
            <asp:ListItem>49</asp:ListItem>
            <asp:ListItem>50</asp:ListItem>
            <asp:ListItem>51</asp:ListItem>
            <asp:ListItem>52</asp:ListItem>
            <asp:ListItem>53</asp:ListItem>
            <asp:ListItem>54</asp:ListItem>
            <asp:ListItem>55</asp:ListItem>
            <asp:ListItem>56</asp:ListItem>
            <asp:ListItem>57</asp:ListItem>
            <asp:ListItem>58</asp:ListItem>
            <asp:ListItem>59</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_END_Minute" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue" Style="z-index: 125; left: 313px; position: absolute; top: 46px">
            <asp:ListItem Value="00">0</asp:ListItem>
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
            <asp:ListItem>24</asp:ListItem>
            <asp:ListItem>25</asp:ListItem>
            <asp:ListItem>26</asp:ListItem>
            <asp:ListItem>27</asp:ListItem>
            <asp:ListItem>28</asp:ListItem>
            <asp:ListItem>29</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem>31</asp:ListItem>
            <asp:ListItem>32</asp:ListItem>
            <asp:ListItem>33</asp:ListItem>
            <asp:ListItem>34</asp:ListItem>
            <asp:ListItem>35</asp:ListItem>
            <asp:ListItem>36</asp:ListItem>
            <asp:ListItem>37</asp:ListItem>
            <asp:ListItem>38</asp:ListItem>
            <asp:ListItem>39</asp:ListItem>
            <asp:ListItem>40</asp:ListItem>
            <asp:ListItem>41</asp:ListItem>
            <asp:ListItem>42</asp:ListItem>
            <asp:ListItem>43</asp:ListItem>
            <asp:ListItem>44</asp:ListItem>
            <asp:ListItem>45</asp:ListItem>
            <asp:ListItem>46</asp:ListItem>
            <asp:ListItem>47</asp:ListItem>
            <asp:ListItem>48</asp:ListItem>
            <asp:ListItem>49</asp:ListItem>
            <asp:ListItem>50</asp:ListItem>
            <asp:ListItem>51</asp:ListItem>
            <asp:ListItem>52</asp:ListItem>
            <asp:ListItem>53</asp:ListItem>
            <asp:ListItem>54</asp:ListItem>
            <asp:ListItem>55</asp:ListItem>
            <asp:ListItem>56</asp:ListItem>
            <asp:ListItem>57</asp:ListItem>
            <asp:ListItem>58</asp:ListItem>
            <asp:ListItem>59</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_ST_Second" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue" Style="z-index: 126; left: 363px; position: absolute; top: 24px">
            <asp:ListItem Value="00">0</asp:ListItem>
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
            <asp:ListItem>24</asp:ListItem>
            <asp:ListItem>25</asp:ListItem>
            <asp:ListItem>26</asp:ListItem>
            <asp:ListItem>27</asp:ListItem>
            <asp:ListItem>28</asp:ListItem>
            <asp:ListItem>29</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem>31</asp:ListItem>
            <asp:ListItem>32</asp:ListItem>
            <asp:ListItem>33</asp:ListItem>
            <asp:ListItem>34</asp:ListItem>
            <asp:ListItem>35</asp:ListItem>
            <asp:ListItem>36</asp:ListItem>
            <asp:ListItem>37</asp:ListItem>
            <asp:ListItem>38</asp:ListItem>
            <asp:ListItem>39</asp:ListItem>
            <asp:ListItem>40</asp:ListItem>
            <asp:ListItem>41</asp:ListItem>
            <asp:ListItem>42</asp:ListItem>
            <asp:ListItem>43</asp:ListItem>
            <asp:ListItem>44</asp:ListItem>
            <asp:ListItem>45</asp:ListItem>
            <asp:ListItem>46</asp:ListItem>
            <asp:ListItem>47</asp:ListItem>
            <asp:ListItem>48</asp:ListItem>
            <asp:ListItem>49</asp:ListItem>
            <asp:ListItem>50</asp:ListItem>
            <asp:ListItem>51</asp:ListItem>
            <asp:ListItem>52</asp:ListItem>
            <asp:ListItem>53</asp:ListItem>
            <asp:ListItem>54</asp:ListItem>
            <asp:ListItem>55</asp:ListItem>
            <asp:ListItem>56</asp:ListItem>
            <asp:ListItem>57</asp:ListItem>
            <asp:ListItem>58</asp:ListItem>
            <asp:ListItem>59</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DP_END_Second" runat="server" BackColor="LightSteelBlue" Font-Size="XX-Small"
            ForeColor="Blue" Style="z-index: 127; left: 363px; position: absolute; top: 47px">
            <asp:ListItem Value="00">0</asp:ListItem>
            <asp:ListItem Value="01">1</asp:ListItem>
            <asp:ListItem Value="02">2</asp:ListItem>
            <asp:ListItem Value="03">3</asp:ListItem>
            <asp:ListItem Value="04">4</asp:ListItem>
            <asp:ListItem Value="05">5</asp:ListItem>
            <asp:ListItem Value="06">6</asp:ListItem>
            <asp:ListItem Value="07">7</asp:ListItem>
            <asp:ListItem Value="08">8</asp:ListItem>
            <asp:ListItem Value="09">9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
            <asp:ListItem>24</asp:ListItem>
            <asp:ListItem>25</asp:ListItem>
            <asp:ListItem>26</asp:ListItem>
            <asp:ListItem>27</asp:ListItem>
            <asp:ListItem>28</asp:ListItem>
            <asp:ListItem>29</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem>31</asp:ListItem>
            <asp:ListItem>32</asp:ListItem>
            <asp:ListItem>33</asp:ListItem>
            <asp:ListItem>34</asp:ListItem>
            <asp:ListItem>35</asp:ListItem>
            <asp:ListItem>36</asp:ListItem>
            <asp:ListItem>37</asp:ListItem>
            <asp:ListItem>38</asp:ListItem>
            <asp:ListItem>39</asp:ListItem>
            <asp:ListItem>40</asp:ListItem>
            <asp:ListItem>41</asp:ListItem>
            <asp:ListItem>42</asp:ListItem>
            <asp:ListItem>43</asp:ListItem>
            <asp:ListItem>44</asp:ListItem>
            <asp:ListItem>45</asp:ListItem>
            <asp:ListItem>46</asp:ListItem>
            <asp:ListItem>47</asp:ListItem>
            <asp:ListItem>48</asp:ListItem>
            <asp:ListItem>49</asp:ListItem>
            <asp:ListItem>50</asp:ListItem>
            <asp:ListItem>51</asp:ListItem>
            <asp:ListItem>52</asp:ListItem>
            <asp:ListItem>53</asp:ListItem>
            <asp:ListItem>54</asp:ListItem>
            <asp:ListItem>55</asp:ListItem>
            <asp:ListItem>56</asp:ListItem>
            <asp:ListItem>57</asp:ListItem>
            <asp:ListItem>58</asp:ListItem>
            <asp:ListItem>59</asp:ListItem>
        </asp:DropDownList>
        &nbsp;
        <asp:Label ID="Label9" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 128;
            left: 299px; position: absolute; top: 26px" Text="时"></asp:Label>
        <asp:Label ID="Label15" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 129;
            left: 299px; position: absolute; top: 47px" Text="时"></asp:Label>
        <asp:Label ID="Label10" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 130;
            left: 351px; position: absolute; top: 27px" Text="分"></asp:Label>
        <asp:Label ID="Label16" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 131;
            left: 351px; position: absolute; top: 47px" Text="分"></asp:Label>
        <asp:Label ID="Label11" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 132;
            left: 401px; position: absolute; top: 26px" Text="秒"></asp:Label>
        <asp:Label ID="Label17" runat="server" Font-Size="Small" ForeColor="Red" Style="z-index: 133;
            left: 401px; position: absolute; top: 47px" Text="秒"></asp:Label>
        &nbsp;
            </form>
</body>
</html>
