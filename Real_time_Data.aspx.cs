﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Odbc;
using System.Data.Sql;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //每10秒刷新 
            MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(100);
        }
        test();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            GridView1.DataSource = Data.GetRealData1();
            GridView1.DataBind();
        }
        catch (OdbcException err)
        {
            TextBox1.Text = err.Message.ToString();
        }
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }

    private void test()
    {
        try
        {
            GridView2.DataSource = Data.GetRealData1();
            GridView2.DataBind();
        }
        catch (OdbcException err)
        {
            TextBox1.Text = err.Message.ToString();
        }
    }
}
