﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;


public partial class RealDataTest : System.Web.UI.Page
{
    Label[] lab = new Label[100];
    DataSet ds;
    IFixData FixData;
    #region 初始化控件
    /// <summary>
   /// 初始化控件
   /// </summary>
   /// <param name="sender"></param>
   /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        lab[0] = Label1; lab[1] = Label2; lab[2] = Label3; lab[3] = Label4; lab[4] = Label5; lab[5] = Label6; lab[6] = Label7;
        lab[7] = Label8; lab[8] = Label9; lab[9] = Label10; lab[10] = Label11; lab[11] = Label12; lab[12] = Label13; lab[13] = Label14;
        lab[14] = Label15; lab[15] = Label16; lab[16] = Label17; lab[17] = Label18; lab[18] = Label19; lab[19] = Label20; lab[20] = Label21;
        lab[21] = Label22; lab[22] = Label23; lab[23] = Label24; lab[24] = Label25; lab[25] = Label26; lab[26] = Label27; lab[27] = Label28;
        lab[28] = Label29; lab[29] = Label30; lab[30] = Label31; lab[31] = Label32; lab[32] = Label33; lab[33] = Label34; lab[34] = Label35;
        lab[35] = Label36; lab[36] = Label37; lab[37] = Label38; lab[38] = Label39; lab[39] = Label40; lab[40] = Label41; lab[41] = Label42;
        lab[42] = Label43; lab[43] = Label44; lab[44] = Label45; lab[45] = Label46; lab[46] = Label47; lab[47] = Label48; lab[48] = Label49;
        lab[49] = Label50; lab[50] = Label51; lab[51] = Label52; lab[52] = Label53; lab[53] = Label54; lab[54] = Label55; lab[55] = Label56;
        lab[56] = Label57; lab[57] = Label58; lab[58] = Label59; lab[59] = Label60; lab[60] = Label61; lab[61] = Label62; lab[62] = Label63;
        lab[63] = Label64; lab[64] = Label65; lab[65] = Label66; lab[66] = Label67; lab[67] = Label68; lab[68] = Label69; lab[69] = Label70;
        lab[70] = Label71; lab[71] = Label72; lab[72] = Label73; lab[73] = Label74; lab[74] = Label75; lab[75] = Label76; lab[76] = Label77;
        lab[77] = Label78; lab[78] = Label79; lab[79] = Label80; lab[80] = Label81; lab[81] = Label82; lab[82] = Label83; lab[83] = Label84;
        lab[84] = Label85; lab[85] = Label86; lab[86] = Label87; lab[87] = Label88; lab[88] = Label89; lab[89] = Label90; lab[90] = Label91;
        lab[91] = Label92; lab[92] = Label93; lab[93] = Label94; lab[94] = Label95; lab[95] = Label96; lab[96] = Label97; lab[97] = Label98;
        lab[98] = Label99; lab[99] = Label100;
        //RealDataTest_init();
        if (!IsPostBack)
        {
            //每10秒刷新 
            MagicAjax.AjaxCallHelper.SetAjaxCallTimerInterval(2000);
            Text1.Value = "heheheheeh!";
        }
        Show();
        //ShowTimer();
        //showstatic();
        //System.GC.Collect();
    }
    #endregion

    private void ShowTimer()
    {
        for (int i = 0; i < 100; i++)
        {
           //lab[i].Text = fixDataTimer.GetRealDataTimer()[i];
        }
    }

    private void Show()
    {
        ds = Data.GetRealData1();
        //ds = FixData.GetRealData();
        for (int i = 0; i < 100; i++)
        {
            lab[i].Text = ds.Tables[0].Rows[i]["A_CV"].ToString();
            //lab[i].Text=IFixDataTimer.data[i];
            //lab[i].Text = Data.GetRealData().Tables[0].Rows[i]["A_CV"].ToString();

        }
        Text1.Value = ds.Tables[0].Rows[50]["A_CV"].ToString();
        //ds.Dispose();
    }

    private void showstatic()
    {
        ds = Data.GetRealData1();
        
        for (int i = 0; i < 100; i++)
        {
            lab[i].Text = ds.Tables[0].Rows[i]["A_CV"].ToString();

        }
        
    }
}
