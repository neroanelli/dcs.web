﻿<%@ Application Language="C#" %>
<%@ import Namespace ="System" %> 
<%@ Import Namespace ="System.Collections.Generic" %>
<%@ Import Namespace ="System.Text" %>
<%@ Import Namespace ="System.Data" %>
<%@ Import Namespace ="System.Timers" %>
<%@ Import Namespace ="System.Data.Odbc" %>
<%@ Import Namespace ="System.Data.Sql" %>
<%@ Import Namespace="System.Configuration" %>

<script runat="server">
    public static System.Timers.Timer T_Timer = new System.Timers.Timer();
    public static System.Timers.Timer T_Timer2 = new System.Timers.Timer();
    //public static DataSet ds =new DataSet();
    public static int Con;//初始为1  可以访问历史数据库；0则对历史数据库加锁
    
    void Application_Start(object sender, EventArgs e) 
    {
         Application.Add("ds",null);
         Application.Add("Con",1);
         
        // 在应用程序启动时运行的代码
        T_Timer.Interval = 1000;
        T_Timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
        T_Timer.Enabled = true;
        T_Timer2.Interval = 10000;
        T_Timer2.Elapsed += new System.Timers.ElapsedEventHandler(timer2_Elapsed);
        T_Timer2.Enabled = true;
        //T_Timer.Start();
        //GC.KeepAlive(T_Timer);//Keep the timer alive until the end of Application.
        Data.OnlineNO = 0;//Clear the online NO count
        com.conch.Init.ClsReadKey.ReadKey();
        com.conch.Init.InitialTooltips.SetTooltips();
     }

    void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
    {
        //ds = Data.GetRealData();
        //string RealConnectionSting = ConfigurationSettings.AppSettings["RealDataConnectionString"];//从web.config中读出数据库连接字符串
        //RealConnectionSting = "DSN=FIX Dynamics Real Time Data";
        //OdbcConnection RealConnection;//实时数据库连接
        //RealConnection = new OdbcConnection(RealConnectionSting);
        //OdbcDataAdapter adapter = new OdbcDataAdapter("select A_CV from FIX", RealConnection);
        //RealConnection.Open();// 11.19日修改
        //adapter.Fill(ds, "FIX");
        //RealConnection.Close();// 11.19日修改
        //adapter.Dispose();
        //Application["ds"]=Data.GetRealData();
        //Data.GetRealData();
    }

    void timer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
    {
        com.conch.Init.ClsReadKey.ReadKey();
        Application["Con"] = 1;
    }


    
    void Application_End(object sender, EventArgs e) 
    {
        //  在应用程序关闭时运行的代码
        
    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // 在出现未处理的错误时运行的代码

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // 在新会话启动时运行的代码
        Data.OnlineNO++;

        Response.Redirect("LoginTest.aspx");
    }

    void Session_End(object sender, EventArgs e) 
    {
        // 在会话结束时运行的代码。 
        // 注意: 只有在 Web.config 文件中的 sessionstate 模式设置为
        // InProc 时，才会引发 Session_End 事件。如果会话模式设置为 StateServer 
        // 或 SQLServer，则不会引发该事件。
        Data.OnlineNO--;
    }

    
       
</script>
