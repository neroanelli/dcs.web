﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>登陆窗口</title>
    <script >
     function window.onload() 
   { 
   var obj=document.getElementById("Login1"); 
   window.resizeTo(obj.offsetWidth +15,obj.offsetHeight +100); 
//   window.resizeTo(screen.Width/4,screen.Height/4); 
   window.moveTo(screen.Width/3,screen.Height/3);
   window.scroll=false;
   } 
    </script>
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Login ID="Login1" runat="server" DestinationPageUrl="~/Map.aspx" Style="z-index: 100;
            left: 5px; position: absolute; top: 38px" BackColor="#EFF3FB" BorderColor="#B5C7DE" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#333333" Height="78px" Width="173px" LoginButtonStyle-Width="30" OnAuthenticate="Login1_Authenticate">
            <TitleTextStyle BackColor="#507CD1" Font-Bold="True" Font-Size="0.9em" ForeColor="White" />
            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
            <TextBoxStyle Font-Size="0.8em" />
            <LoginButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid" BorderWidth="1px"
                Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" Width="30px" />
            <CheckBoxStyle HorizontalAlign="Justify" />
        </asp:Login>
    
    </div>
        &nbsp; &nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Height="24px" Style="z-index: 102;
            left: 7px; position: absolute; top: 7px" Text='如需浏览历史趋势，请先以管理员身份登录'
            Width="178px" Font-Size="Small"></asp:Label>
    </form>
</body>
</html>
