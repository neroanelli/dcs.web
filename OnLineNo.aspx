﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnLineNo.aspx.cs" Inherits="OnLineNo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>在线人员</title>
    <script >
     function window.onload() 
   { 
   window.resizeTo(150,500); 
   window.scroll=false;
   } 
    </script>
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="GridView1" runat="server" Style="z-index: 100;
            left: 4px; position: absolute; top: 6px" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" >
            <Columns>
                <asp:BoundField DataField="Username" HeaderText="用户" />
                <asp:BoundField DataField="IsOnline" HeaderText="在线" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" BorderColor="Transparent" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
